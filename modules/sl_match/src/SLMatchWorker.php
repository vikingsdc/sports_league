<?php

namespace Drupal\sl_match;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Config\ConfigFactory;
use \Drupal\Component\Utility\Timer;

class SLMatchWorker {

  protected $efq;

  /**
   * When the service is created, set a value for the example variable.
   */
  public function __construct(ConfigFactory $config_factory, QueryFactory $efq) {
    $this->efq = $efq;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity.query')
    );
  }

  public function compute($match_id) {

    if (!is_numeric($match_id)) {
      return ;
    }

    $node_manager = \Drupal::entityTypeManager()->getStorage('node');
    $moments_manager = \Drupal::entityTypeManager()->getStorage('sl_match_moments');
    $rosters_manager = \Drupal::entityTypeManager()->getStorage('sl_rosters');

    $node = $node_manager->load($match_id);

    $efq = \Drupal::entityQuery('sl_match_moments');
    $efq->condition('type', 'sl_match_moments_substitution');
    $efq->condition('field_sl_match', $match_id);
    $subs_ids =  $efq->execute();

    $in = $out = [];
    if (!empty($subs_ids)) {
      $substitutions = $moments_manager->loadMultiple($subs_ids);
      foreach ($substitutions as $sub) {
        if (!empty($sub->field_sl_match_moments_player->target_id) && !empty($sub->field_sl_match_moments_player_in->target_id)) {
          $out[$sub->field_sl_match_moments_player->target_id] = $sub->field_sl_match_moments_time->value;
          $in[$sub->field_sl_match_moments_player_in->target_id] = $sub->field_sl_match_moments_time->value;
        }
      }
    }

    $efq = \Drupal::entityQuery('sl_rosters');
    $efq->condition('type', 'sl_rosters');
    $efq->condition('field_sl_match', $match_id);
    $rosters_ids = $efq->execute();

    $rosters = $rosters_manager->loadMultiple($rosters_ids);

    foreach($rosters as $roster) {
      if (in_array($roster->field_sl_roster_player->target_id, array_keys($out))) {
        $roster->set('field_sl_roster_out', $out[$roster->field_sl_roster_player->target_id]);
        $roster->save();
      }
      if (in_array($roster->field_sl_roster_player->target_id, array_keys($in))) {
        $roster->set('field_sl_roster_in', $in[$roster->field_sl_roster_player->target_id]);
        $roster->set('field_sl_roster_out', 90);
        $roster->save();
      }

      // compute all players
      if (is_numeric($roster->field_sl_roster_player->target_id)) {
        $player = $node_manager->load($roster->field_sl_roster_player->target_id);
        $player->save();
      }
    }

    return $node;
  }

}
